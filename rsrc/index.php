<html>
     <head>
    	<title>Archive</title>
        <style>
        	body { text-align: center; }
            ul { list-style: none; display: inline-block; }
            li { margin-bottom: 10px; } 
            a, a:visited { text-decoration: none; color: #3498DC;}
      	</style>
        <link rel="shortcut icon" type="image/png" href="../css/logo.png"/>
          <script src="../js/jquery.min.js"></script>
    </head>
    <body>
    	<h3>"Free Software, Free Thoughts" 's Archive</h3>



<?php

   /*
   Copyright (C) 2014, 2015  Giuseppe D`Amico <jhvost@outlook.com>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
   
function ListFolder($path) {
    $dir_handle = @opendir($path) or die("Unable to open $path");
    $dirname = end(explode("/", $path));
    echo "<ul id='ita'><h2>Italiano:</h2></ul>";
    echo "<ul id='eng'><h2>English:</h2></ul>";
    
    while (false !== ($file = readdir($dir_handle))) {
        if($file!="." && $file!=".." && 
           substr($file,-3,3)!="php") {
            if (is_dir($path."/".$file)) { ListFolder($path."/".$file); }
            else {
            	$files = array(".fsft", ".pdf", "_eng");
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $eng = array();
                $ita = array();
            	if (stripos($file, "main") !== false) {/*do nothing */} else
                if (stripos($file, "_eng") !== false) { array_push($eng, $file); } else {
                array_push($ita, $file); 
                }
                
                foreach ($ita as $key=>$value) {
                	$clean = str_replace($files, '', $value);
                	?> 
                   <script>
		     /*
		     @licstart  The following is the entire license notice for the 
		     JavaScript code in this page.

		     Copyright (C) 2015  Giuseppe 'jHvost' D`Amico

		     The JavaScript code in this page is free software: you can
		     redistribute it and/or modify it under the terms of the GNU
		     General Public License (GNU GPL) as published by the Free Software
		     Foundation, either version 3 of the License, or (at your option)
		     any later version.  The code is distributed WITHOUT ANY WARRANTY;
		     without even the implied warranty of MERCHANTABILITY or FITNESS
		     FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
   
		     As additional permission under GNU GPL version 3 section 7, you
		     may distribute non-source (e.g., minimized or compacted) forms of
		     that code without the copy of the GNU GPL normally required by
		     section 4, provided you include this license notice and a URL
		     through which recipients can access the Corresponding Source.
   
		     @licend  The above is the entire license notice
		     for the JavaScript code in this page. 
		     */
                    	var lol = '<? echo "$clean ($ext)";?>'
                        var link = '<? echo $file; ?>'
                    	$('ul#ita').append("<li><a href='"+link+"'>"+lol+"</a></li>")
                    </script>
                    <?
                    }
                foreach ($eng as $key=>$value) {
                	$clean_eng = str_replace($files, '', $value)
                	?> 
                    <script>
                    	
                    	var lol1 = '<? echo "$clean_eng ($ext)";?>'
                        var link1 = '<? echo $file; ?>'
                    	$('ul#eng').append("<li><a href='"+link1+"'>"+lol1+"</a></li>")
                    </script>
                    <?
                    }
            }
        }
    }
   
    //closing the directory
    closedir($dir_handle);
}

ListFolder("./")
?>
	</body>
</html>
    
