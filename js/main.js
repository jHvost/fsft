/* 
   @licstart  The following is the entire license notice for the 
   JavaScript code in this page.

   Copyright (C) 2015  Giuseppe 'jHvost' D`Amico

   The JavaScript code in this page is free software: you can
   redistribute it and/or modify it under the terms of the GNU
   General Public License (GNU GPL) as published by the Free Software
   Foundation, either version 3 of the License, or (at your option)
   any later version.  The code is distributed WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
   
   As additional permission under GNU GPL version 3 section 7, you
   may distribute non-source (e.g., minimized or compacted) forms of
   that code without the copy of the GNU GPL normally required by
   section 4, provided you include this license notice and a URL
   through which recipients can access the Corresponding Source.
   
   @licend  The above is the entire license notice
   for the JavaScript code in this page. 
*/

$(document).ready(function() {
    var language = 0;
    $("#current-article>p").load("rsrc/main.fsft content");
    $("#current-article>h2").load("rsrc/main.fsft post_title");
    $("#language").click(function(){
				    if (language == 1) { language = 0; } else { language = 1;}
				    if (language == 1) {
					$("#current-article>p").empty().load("rsrc/main_eng.fsft content");
					$("#current-article>h2").empty().load("rsrc/main_eng.fsft post_title");
					$("#language>p").empty().append("Cambia lingua del sito web: ");
					$("#language>a").empty().append("Italiano");
				    } else if (language == 0) {
					$("#current-article>p").empty().load("rsrc/main.fsft content");
					$("#current-article>h2").empty().load("rsrc/main.fsft post_title");
					$("#language>p").empty().append("Change website language: ");
					$("#language>a").empty().append("English");
				    }
    });
});
